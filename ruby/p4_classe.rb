class Destination
    def initialize(n="", p="", j=0)
        @nom = n
        @pays = p
        @jours = j
    end

    def afficher
        print "%s (%s) : %dj\n" % [@nom, @pays, @jours]        
    end

    attr_accessor :nom, :pays, :jours
end

class Destination
    def allonger(j=1)
        @jours += j
    end

    def raccourcir(j=1)
        allonger -j
    end
end

class DestinationMaritime < Destination
    def initialize(n="", p="", j=0, i="")
        super(n, p, j)
        @ile = i
    end

    def afficher
        super
        print "%s\n" % @ile        
    end
end