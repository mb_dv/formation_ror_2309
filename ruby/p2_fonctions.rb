def afficher_presentation
    print "Voyages au Mexique\n"
end

def afficher_avertissement(lieu, point=".")
    print "Ce lieu est dangeureux : #{lieu} #{point}\n"
end

def afficher_avertissement2(lieu:, point:".")
    print "Ce lieu est dangeureux : #{lieu} #{point}\n"
end

def calculer_prix(jours)
    return 470 + jours * 34
end

afficher_presentation
afficher_avertissement "Chihuahua", "!" # Chihuahua !
afficher_avertissement "Mexico" # Mexico .
afficher_avertissement2 point:"!!", lieu:"Teotihuacan"
print "Pour 10 jours : %d€" % calculer_prix(10)