# Programme 1 : accueil et syntaxe
# nom = gets
# print "Bonjour "+nom

=begin
print ( 
    "Bonjour"
)
=end

#Voyages au Canada : -20% sur 860e d'avion et 7j à 48e
promo = 20
avion = 860
print "Jours ? "
duree = gets.to_i
hotel = 48

prix_total = (1-promo/100.0) * (avion+duree*hotel)
# print "Voyage au Canada : " + prix_total.to_s + "e"
# print "Voyage au Canada : #{prix_total}e"
print "Voyage au Canada : %.2f euros\n" % prix_total

# Air Canada en général, sauf les voyages courts (moins de 7 jours) avec
# Québec'Air et les voyages régulier (7j, 14j, 21j...) avec Air Transat
if duree < 7
    print "Québec'Air\n"
elsif duree % 7 == 0
    print "Air Transat\n"
else
    print "Air Canada\n"
end

# Programme :
# Jour 1 : Avion
# Jour 2 : Rando
# Jour 3 : Rando
# Jour 4 : Rando
# Jour 5 : Poutine
# Jour 6 : Rando
# Jour 7 : Rando
# Jour 8 : Avion
occupations = []
(1..duree).each do |j|
    if (j==1 or j==duree)
        occupation = "Avion"
    elsif j%4 == 1
        occupation = "Poutine"
    else 
        occupation = "Rando"
    end
    print "Jour %d : %s\n" % [j, occupation]
    occupations[j] = occupation
end

nb_randos = occupations.select { |x| x=="Rando" }.count
print "Nombre de randos : %d\n" % nb_randos

repas = [
    {:prix => 11, :contenu => {:entree => "Frites", :plat => "Frites", :boisson => "Frites mixées" } },
    {:prix => 24.5, :contenu => {:entree => "Homard", :plat => "Poutine", :boisson => "Soda" } }
]
# Afficher le plat du 2ème repas
print "Plat du 2ème repas : %s\s" % repas[1][:contenu][:plat]