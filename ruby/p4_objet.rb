# require "./p4_classe.rb"
require_relative "p4_classe.rb"

d1 = Destination.new
d1.nom = "Madrid"
d1.pays = "Espagne"
d1.jours = 4
d1.afficher

d2 = Destination.new "Cordoue", "Espagne", 2
d2.allonger 2
d2.raccourcir 3
d2.afficher

dm1 = DestinationMaritime.new "Tenerife", "Espagne", 3, "Grande Canarie"
dm1.afficher
