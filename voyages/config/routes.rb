Rails.application.routes.draw do
  get 'presentation/index'
  get 'presentation/accueil', to: 'presentation#index'
  get 'presentation/Default', controller: 'presentation', action: :index
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  get 'presentation/mentions'
  get 'presentation/endirect'
  get 'presentation/horaires'
  
  # get 'voyages', to: 'voyages#index'
  # get 'voyages/:id', to: 'voyages#show'
  resources :voyages

  # Defines the root path route ("/")
  # root "articles#index"
  root "presentation#index"
  # pas : root "presentation#accueil"

end
