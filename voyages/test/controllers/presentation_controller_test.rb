require "test_helper"

class PresentationControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get presentation_index_url
    assert_response :success
  end
end
