class CreateVoyages < ActiveRecord::Migration[7.0]
  def change
    create_table :voyages do |t|
      t.text :nom, :null=>false
      t.decimal :prix, :precision=>8, :scale=>2

      t.timestamps
    end
  end
end
