# rails generate model Inscription nom:string tel:string voyage:references
class CreateInscriptions < ActiveRecord::Migration[7.0]
  def change
    create_table :inscriptions do |t|
      t.string :nom
      t.string :tel
      t.references :voyage, null: false, foreign_key: true

      t.timestamps
    end
  end
end
