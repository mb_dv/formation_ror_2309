class PresentationController < ApplicationController
  def index
  end

  def mentions
=begin
    render "vue2"
    render :index
    render "controleur2/action"
    render ..., :layout=>'lay2' 
    render ..., :status=>403
    render :file=>".....erb"
    render :plain=>"...."
    render :html=>"<p>...</p>", :layout=>true
    render :inline=>"<p><%=...%></p>"
    redirect_to "/ctrl/action", status: 301
    redirect_to "https://....."
    head :not_found, date: ...., connection: ....
    head :ok
=end
  end

  # http://..../presentation/endirect
  # 21h-9h : message "c'est la nuit, rien de visible"
  # 9h-12h : afficher en plein écran (layout simplifié) une image d'océan
  # sinon : renvoyer vers un autre site : explore.org/livecams/....
  def endirect
    heure = Time.now.hour
    if heure>=21 || heure<9
      render :html=>"<p>c'est la nuit, rien de visible</p>"
    elsif heure<12
      @img2 = "ocean.jpg"
      render :layout => "image"
    else
      redirect_to "https://explore.org/livecams/sunsets/turtle-bay-east-sunset", :allow_other_host=> true
    end
  end

  def horaires
    @horaires = [ # 1 = lundi
      nil,
      {:ouverture=>10, :fermeture=>18},
      nil,
      {:ouverture=>10, :fermeture=>18},
      {:ouverture=>10, :fermeture=>18},
      {:ouverture=>10, :fermeture=>18},
      {:ouverture=>9, :fermeture=>20},
      {:ouverture=>10, :fermeture=>12}
    ]
    @jours = [nil,"L","M","M","J","V","S","D"]
  end

end
