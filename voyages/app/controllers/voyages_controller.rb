class VoyagesController < ApplicationController
    # get 'voyages', to: 'voyages#index'
    def index
        @voyages = Voyage.all
    end

    # get 'voyages/:id', to: 'voyages#show'
    def show
        begin
            @voyage = Voyage.find params[:id]
        rescue ActiveRecord::RecordNotFound
            render :file => Rails.root.to_s + "/public/404.html", :status => 404
        end
    end

    def new
        @voyage = Voyage.new
        @voyage.prix = 1000
    end

    def create
        # @voyage = Voyage.new params[:voyage].permit(:nom, :prix)
        @voyage = Voyage.new params.require(:voyage).permit(:nom, :prix)      
        if @voyage.save
            logger.info "Sauvegarde de voyage %d" % @voyage.id
            redirect_to @voyage
        else
            render :new
        end
    end
end
