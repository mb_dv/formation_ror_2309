class Voyage < ApplicationRecord
    has_many :inscriptions
    validates :nom, :presence => true, :length => { :in => 5..50 }
    validates :prix, :presence => true, :numericality => { :in => 100..10000 }
end
